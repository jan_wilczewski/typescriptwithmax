// // string
// let myName: string = 'Max';
//
// //  number
// // (there is just one type number no matter
// // if it is int, float or double
// let myAge: number = 32.5;
//
// // boolean
// let hasHobbies: boolean = true;
//
// // assign types
// let myRealAge: number;
// myRealAge = 27;
// // myRealAge = '27';
//
// // array
// let hobbies: any[] = ["Cooking", "Sports"];
// hobbies = [100];
//
// // tuples - it is an array with variables of mixed types inside
// // the order of defined type and added data must match
// let address: [string, number] = ["Superstreet", 99];
//
// // enum
// enum Color {
//     Gray,  // 0
//     Green = 100, // 100
//     Blue   // 2
// }
//
// let myColor: Color = Color.Green;
// console.log(myColor);
//
// // any
// let car: any = "BMW";
// console.log(car);
// car = { brand: "BMW", series: 3 };
// console.log(car);
//
// // functions
// function returnMyName(): string {
//     return myName;
// }
//
// // void
// function  sayHello(): void {
//     console.log("Hello")
// }
//
// // function with argument types
// function multiply(value1: number, value2: number): number {
//     return value1 * value2;
// }
// // console.log(multiply(2, "Max"));
// console.log(multiply(2, 10));
//
// // function types
// let myMultiply: (a: number, b: number) => number;
// // myMultiply = sayHello;
// // myMultiply();
// myMultiply = multiply;
// // myMultiply();
//
//
// // objects
// let userData: { name: string, age: number } = {
//     name: "Max",
//     age: 27
// };
//
// // userData = {
// //     a: "Hello",
// //     b: 33
// // };
//
// // complex object
// let complex: {data: number[], output: (all: boolean) => number[]} = {
//     data: [100, 3.99, 10],
//     output: function (all: boolean): number[] {
//         return this.data;
//     }
// };
//
// // complex {}
// // type alias
//
// type Complex = {data: number[], output: (all: boolean) => number[]};
//
// let complex2: Complex = {
//     data: [100, 3.99, 10],
//     output: function (all: boolean): number[] {
//         return this.data;
//     }
// };
//
// // union types
// let myRealAge2: number | string = 27;
// myRealAge2 = "27";
// // myRealAge2 = true;
//
// // check types
// let  finalValue = 30;
// if (typeof finalValue == "number") {
//     console.log("Final value is a number.")
// }
//
// // never
// function neverReturns(): never {
//     throw new Error('An error!')
// }
//
// // Nullable types
// let canBeNull: number | null = 12;
// canBeNull = null;
// let canAlsoBeNull;
// canAlsoBeNull = null;
// let canThisBeAny = null;
