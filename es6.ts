// let $ const
let variable = "Test";
console.log(variable);
variable = "Another value";
console.log(variable);

const maxLevels = 100;
console.log(maxLevels);
// maxLevels = 99;

// Block scope
function reset() {
    let variable = null;
    console.log(variable);
}
console.log(variable);